"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "BasicInput", {
  enumerable: true,
  get: function get() {
    return _BasicInput.default;
  }
});
Object.defineProperty(exports, "TextArea", {
  enumerable: true,
  get: function get() {
    return _TextArea.default;
  }
});
var _BasicInput = _interopRequireDefault(require("./components/BasicInput"));
var _TextArea = _interopRequireDefault(require("./components/TextArea"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }