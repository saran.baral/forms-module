import React from "react";
import PropTypes from "prop-types";

const BasicInput = (props) => {
    let {
        name,
        onChange,
        label,
        value,
        error,
        type,
        disabled,
        placeholder,
        className,
        min,
        onFocus,
        autoComplete,
        required,
        ref,
        ...others
    } = props;

    return (
        <div className={`mb-3 ${className}`}>
            {label && (
                <label htmlFor={label} className="form-label">
                    {label} {required && <span className="text-danger">*</span>}
                </label>
            )}
            <input
                className={`form-control ${error ? "is-invalid" : ""}`}
                name={name}
                min={min}
                onFocus={onFocus}
                value={value}
                type={type ? type : "text"}
                onChange={onChange}
                id={name}
                required={required}
                disabled={disabled}
                autoComplete={autoComplete}
                placeholder={placeholder}
                ref={ref}
                {...others}
            />
            <div
                id="invalidBox"
                data-testid="invalidBox"
                className="invalidBox"
            >
                {error && <small className="text-danger">{error}</small>}
            </div>
        </div>
    );
};

BasicInput.propTypes = {
    name: PropTypes.string,
    value: PropTypes.any,
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.any.isRequired,
    type: PropTypes.string,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    className: PropTypes.string,
    onFocus: PropTypes.func,
    autoComplete: PropTypes.string,
    min: PropTypes.number,
    required: PropTypes.bool,
    ref: PropTypes.any,
};

export default BasicInput;
