import React from "react";
import PropTypes from "prop-types";

const TextArea = (props) => {
    let {
        name,
        onChange,
        label,
        value,
        error,
        disabled,
        placeholder,
        className,
        onFocus,
        required,
        ref,
        ...others
    } = props;

    return (
        <div className={`mb-3 ${className}`}>
            {label && (
                <label htmlFor={label} className="form-label">
                    {label} {required && <span className="text-danger">*</span>}
                </label>
            )}
            <textarea
                className={`form-control ${error ? "is-invalid" : ""}`}
                name={name}
                onFocus={onFocus}
                value={value}
                onChange={onChange}
                id={name}
                required={required}
                disabled={disabled}
                placeholder={placeholder}
                ref={ref}
                {...others}
            />
            <div
                id="invalidBox"
                data-testid="invalidBox"
                className="invalidBox"
            >
                {error && <small className="text-danger">{error}</small>}
            </div>
        </div>
    );
};

TextArea.propTypes = {
    name: PropTypes.string,
    value: PropTypes.any,
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.any.isRequired,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    className: PropTypes.string,
    onFocus: PropTypes.func,
    required: PropTypes.bool,
    ref: PropTypes.any,
};

export default TextArea;
